const { redrawing } = await import('./main.js')

export function render(data) {
    const container = document.createElement('div');
    container.classList.add(
        'container',
        'd-flex',
        'justify-content-between',
        'flex-wrap',
        'py-4'
    )

    for (const episode of data.results) {
        const episodeCard = document.createElement('div'),
            cardBody = document.createElement('div'),
            title = document.createElement('h5'),
            text = document.createElement('p'),
            detailsBtn = document.createElement('a');

            episodeCard.style.width = '18%'
            episodeCard.classList.add('card', 'my-2')
            cardBody.classList.add('card-body', 'd-inline-flex', 'justify-content-between', 'flex-column')
            title.classList.add('card-title')
            text.classList.add('card-text')
            detailsBtn.classList.add('btn', 'btn-primary')

            episodeCard.append(cardBody)
            cardBody.append(title)
            cardBody.append(text)
            cardBody.append(detailsBtn)

            title.textContent = episode.title
            text.textContent = `Эпизод номер: ${episode.episode_id}`
            detailsBtn.textContent = 'Подробнее'
            detailsBtn.href = `?films=${data.results.indexOf(episode) + 1}`

            detailsBtn.addEventListener('click', (e) => {
                e.preventDefault()
                const urlRender = history.pushState(null, '', detailsBtn.href),
                    searchParams = new URLSearchParams(location.search),
                    episodeId = searchParams.get('films');
                redrawing(episodeId)
            })

            container.append(episodeCard)
    }

    return container;
}