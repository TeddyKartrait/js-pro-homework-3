const cssPromises = {}
let arrPlanets = [],
    arrSpecies = [];

function loadResource(src) {
    if (src.endsWith('.js')) {
        return import(src);
    }

    if (src.endsWith('.css')) {
        if (!cssPromises[src]) {
            const link = document.createElement('link')
            link.rel = 'stylesheet'
            link.href = src;
            cssPromises[src] = new Promise(resolve => {
                link.addEventListener('load', () => resolve())
            })
            document.head.append(link)
        }
        return cssPromises[src]
    }

    return fetch(src).then(res => res.json())
}

const appContainer = document.getElementById('app'),
    searchParams = new URLSearchParams(location.search);
let episodeId = searchParams.get('films');

function renderPage(moduleName, apiURL, css) {
    Promise.all([moduleName, apiURL, css].map(src => loadResource(src)))
        .then(([pageModule, data]) => {
            appContainer.innerHTML = ''
            if (episodeId) {
                appContainer.append(pageModule.render(data, arrPlanets, arrSpecies));
            } else appContainer.append(pageModule.render(data));
        });
}

async function renderPlanets(url) {
    Promise.all([url].map(src => loadResource(src)))
        .then(([data]) => {
            arrPlanets = []
            for(let planet of data.planets) {
                Promise.all([planet].map(src => loadResource(src)))
                    .then(([data]) => {
                        arrPlanets.push(data.name)
                });
            } 
        });
}

async function renderSpecies(url) {
    Promise.all([url].map(src => loadResource(src)))
        .then(([data]) => {
            arrSpecies = []
            for(let race of data.species) {
                Promise.all([race].map(src => loadResource(src)))
                    .then(([data]) => {
                        arrSpecies.push(data.name)
                });
            } 
        });
}

export async function redrawing(num) {
    await renderPlanets(`https://swapi.dev/api/films/${num}`);
    await renderSpecies(`https://swapi.dev/api/films/${num}`);

    episodeId = num
        renderPage(
            './episode-details.js',
            `https://swapi.dev/api/films/${num}`,
            'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css'
    );
}

window.addEventListener('popstate', (e) => {
    e.preventDefault()

    if (window.location.search != '') {
        renderPlanets(`https://swapi.dev/api/films/${episodeId}`);
        renderSpecies(`https://swapi.dev/api/films/${episodeId}`);
        renderPage(
            './episode-details.js',
            `https://swapi.dev/api/films/${episodeId}`,
            'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css'
        );
    } else {
        renderPage(
            './episode-list.js',
            'https://swapi.dev/api/films',
            'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css'
        );
    }
})



if (episodeId) {
    await renderPlanets(`https://swapi.dev/api/films/${episodeId}`);
    await renderSpecies(`https://swapi.dev/api/films/${episodeId}`);
    renderPage(
        './episode-details.js',
        `https://swapi.dev/api/films/${episodeId}`,
        'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css'
    );
} else {
    renderPage(
        './episode-list.js',
        'https://swapi.dev/api/films',
        'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css'
    );
}

