export function render(data, arr1, arr2) {

    const container = document.createElement('div'),
        title = document.createElement('h1'),
        btn = document.createElement('a'),
        description = document.createElement('p'),
        titlePlanet = document.createElement('h2'),
        listPlanet = document.createElement('ul'),
        titleSpecies = document.createElement('h2'),
        listSpecies = document.createElement('ul');

        container.classList.add('container', 'py-4')
        btn.classList.add('btn', 'btn-primary')
        btn.type = 'button'
        container.append(title, description, titlePlanet, listPlanet, titleSpecies, listSpecies, btn)

        for (const planet of arr1) {
            const item = document.createElement('li')

            item.textContent = planet
            listPlanet.append(item)
        }

        for (const race of arr2) {
            const item = document.createElement('li')

            item.textContent = race
            listSpecies.append(item)
        }

        title.textContent = `${data.title}  Episode: ${data.episode_id}`
        btn.textContent = 'Back to episodes'
        description.textContent = data.opening_crawl
        titlePlanet.textContent = 'Planets:'
        titleSpecies.textContent = 'Species:'

        btn.addEventListener('click', () => {
            history.back()
        })

        return container
}